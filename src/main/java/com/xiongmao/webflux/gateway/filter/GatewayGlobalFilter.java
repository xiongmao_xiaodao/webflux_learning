package com.xiongmao.webflux.gateway.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @Author: lao_hu
 * @Date: 2022/2/25 16:00
 * @Description: 全局的路由拦截，在这里可以实现全局信息的处理
 */
@Component
public class GatewayGlobalFilter implements GlobalFilter {

    private static final Logger logger = LoggerFactory.getLogger(GatewayGlobalFilter.class);

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        // TODO
        return chain.filter(exchange);
    }
}
