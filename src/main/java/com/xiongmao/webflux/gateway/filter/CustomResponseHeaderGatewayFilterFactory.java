package com.xiongmao.webflux.gateway.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import static org.springframework.cloud.gateway.support.GatewayToStringStyler.filterToStringCreator;

/**
 * @Author: lao_hu
 * @Date: 2022/2/25 16:01
 * @Description: 针对路由到的项目做拦截处理
 * @Description: 规则：在路由配置中，拦截器的名称 为 CustomResponseHeader  后面的GatewayFilterFactory 为统一配置，不要加
 */
@Component
public class CustomResponseHeaderGatewayFilterFactory extends AbstractGatewayFilterFactory<AbstractGatewayFilterFactory.NameConfig> {
    private static final Logger logger = LoggerFactory.getLogger(CustomResponseHeaderGatewayFilterFactory.class);
    public CustomResponseHeaderGatewayFilterFactory() {
        super(NameConfig.class);
    }
    @Override
    public GatewayFilter apply(NameConfig config) {
        return new GatewayFilter() {
            @Override
            public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
                return chain.filter(exchange);
            }
            @Override
            public String toString() {
                return filterToStringCreator(CustomResponseHeaderGatewayFilterFactory.this)
                        .append("name", config.getName()).toString();
            }
        };
    }
}
