package com.xiongmao.webflux.auth.controller;

import com.xiongmao.webflux.auth.entity.vo.SysUserVo;
import com.xiongmao.webflux.common.ResponseData;
import com.xiongmao.webflux.common.language.ErrorMsgEnum;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: lao_hu
 * @Date: 2022/2/25 16:35
 * @Description:
 */
@RequestMapping("/test")
@RestController
public class TestController {

    @GetMapping("/exception")
    public ResponseData exception() {
        SysUserVo vo = new SysUserVo();
        vo.getEmail().split("abc");
        return ResponseData.ok();
    }

    @GetMapping("/language")
    public ResponseData language() {
        SysUserVo vo = new SysUserVo();
        vo.setEmail("1");
        return ResponseData.ok(vo, ErrorMsgEnum.SUCCESS.getMsg());
    }
}
