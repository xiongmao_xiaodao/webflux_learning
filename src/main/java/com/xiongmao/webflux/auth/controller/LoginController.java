package com.xiongmao.webflux.auth.controller;

import com.xiongmao.webflux.auth.entity.vo.LoginUserInfo;
import com.xiongmao.webflux.auth.service.SysUserService;
import com.xiongmao.webflux.common.ResponseData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @Author: lao_hu
 * @Date: 2022/2/25 14:56
 * @Description: 登录相关接口
 */
@RequestMapping(value = "/auth")
@RestController
public class LoginController {

    @Autowired
    private SysUserService sysUserService;
    /**
     * 通过邮箱注册
     */
    @PostMapping(value = "/registerByEmail")
    public ResponseEntity<ResponseData> registerByEmail(@RequestBody LoginUserInfo userInfo) {
        return sysUserService.registerByEmail(userInfo);
    }
    /**
     * 通过手机号注册
     */
    @PostMapping(value = "/registerByPhone")
    public ResponseEntity<ResponseData> registerByPhone(@RequestBody LoginUserInfo userInfo) {
        return sysUserService.registerByPhone(userInfo);
    }
    /**
     * json 登录
     */
    @PostMapping(value = "/doLogin")
    public ResponseEntity<ResponseData> doLogin(@RequestBody LoginUserInfo userInfo) {
        return sysUserService.doLogin(userInfo);
    }
    /**
     * form 表单登录
     */
    @PostMapping(value = "/formLogin")
    public Mono<ResponseEntity<ResponseData>> formLogin(ServerWebExchange exchange) {
        return sysUserService.formLogin(exchange);
    }

}
