package com.xiongmao.webflux.auth.controller;

import com.xiongmao.webflux.auth.service.FileService;
import com.xiongmao.webflux.common.ResponseData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * @Author: lao_hu
 * @Date: 2022/2/25 15:12
 * @Description: webflux 框架下的文件上传下载处理 （应该有更好的处理方式,目前只是做到了可上传，响应会比较慢，应该可以通过异步处理更快的响应）
 */
@RequestMapping("/file")
@RestController
public class FileController {

    @Autowired
    private FileService fileService;
    /**
     * 单文件上传
     */
    @PostMapping("/singleUpload")
    public Mono<ResponseData> singleUpload(@RequestPart("file") Mono<FilePart> file) {
        return fileService.singleUpload(file);
    }
    /**
     * 多文件上传
     */
    @PostMapping("/moreUpload")
    public Mono<ResponseData> moreUpload(@RequestPart("file") Flux<FilePart> file) {
        return fileService.moreUpload(file);
    }
    /**
     * 下载 （不完善，会在项目中生成一个文件）
     */
    @GetMapping("/download")
    public Mono<Void> download(@RequestParam("fileName") String fileName, ServerHttpResponse response) {
        return fileService.download(fileName,response);
    }
}
