package com.xiongmao.webflux.auth.entity.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author: lao_hu
 * @Date: 2022/2/25 14:58
 * @Description:
 */
@Data
public class LoginUserInfo implements Serializable {

    private static final long serialVersionUID = 2310506584516626030L;
    /**
     * 登录名
     */
    private String username;
    /**
     * 用户邮箱
     */
    private String email;
    /**
     * 手机号码
     */
    private String tel;
    /**
     * 密码
     */
    private String password;
    /**
     * 验证码
     */
    private String code;

}
