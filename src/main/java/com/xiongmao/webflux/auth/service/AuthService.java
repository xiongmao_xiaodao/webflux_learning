package com.xiongmao.webflux.auth.service;

/**
 * @Author: lao_hu
 * @Date: 2022/2/25 15:35
 * @Description:
 */
public interface AuthService {
    boolean permission(String sid, String url);
    boolean isExitAndFrash(String sid);
}
