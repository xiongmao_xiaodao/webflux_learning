package com.xiongmao.webflux.auth.service;

import com.xiongmao.webflux.common.ResponseData;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.http.server.reactive.ServerHttpResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * @Author: lao_hu
 * @Date: 2022/2/25 15:16
 * @Description:
 */
public interface FileService {
    Mono<ResponseData> singleUpload(Mono<FilePart> file);

    Mono<ResponseData> moreUpload(Flux<FilePart> file);

    Mono<Void> download(String fileName, ServerHttpResponse response);
}
