package com.xiongmao.webflux.auth.service;

import com.xiongmao.webflux.auth.entity.SysUser;
import com.xiongmao.webflux.auth.entity.vo.LoginUserInfo;
import com.xiongmao.webflux.common.ResponseData;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 * 用户信息表(SysUser)表服务接口
 *
 * @author makejava
 * @since 2022-02-25 14:34:41
 */
public interface SysUserService {

    ResponseEntity<ResponseData> doLogin(LoginUserInfo userInfo);

    Mono<ResponseEntity<ResponseData>> formLogin(ServerWebExchange exchange);

    ResponseEntity<ResponseData> registerByEmail(LoginUserInfo userInfo);

    ResponseEntity<ResponseData> registerByPhone(LoginUserInfo userInfo);

}