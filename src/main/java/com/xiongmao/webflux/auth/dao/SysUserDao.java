package com.xiongmao.webflux.auth.dao;

import com.xiongmao.webflux.auth.entity.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * 用户信息表(SysUser)表数据库访问层
 *
 * @author makejava
 * @since 2022-02-25 14:34:36
 */
@Mapper
public interface SysUserDao {
    SysUser selectUserInfo(String userName);
}