package com.xiongmao.webflux.common.language;

import org.springframework.context.i18n.LocaleContextHolder;

/**
 * @Author: lao_hu
 * @Date: 2022/2/25 14:17
 * @Description: 错误码枚举类
 */
public enum ErrorMsgEnum {
    UPLOAD_ERROR(10003,"upload error","上传失败"),
    /**
     * 检查认证，用户名或密码错误
     */
    CHECK_YOUR_CREDENTIALS(10002,"check certification","户名或密码错误"),
    /**
     * 失败
     */
    FAIL(10001,"FAIL","失败"),
    /**
     * 成功
     */
    SUCCESS(10000,"SUCCESS","成功");

    private int code;
    private String msg;
    private String enMsg;

    ErrorMsgEnum(int code, String enMsg, String msg) {
        this.code = code;
        this.enMsg = enMsg;
        this.msg = msg;
    }

    public String getMsg() {
        String language = LocaleContextHolder.getLocale().getLanguage();
        if("en".equals(language)){
            return enMsg;
        }
        return msg;
    }
    public int getCode() {
        return code;
    }
}
