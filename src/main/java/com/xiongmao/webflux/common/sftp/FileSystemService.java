package com.xiongmao.webflux.common.sftp;

import java.io.File;
import java.io.InputStream;

/**
 * @Author: lao_hu
 * @Date: 2022/2/25 14:13
 * @Description:
 */
public interface FileSystemService {
    boolean uploadFile(String targetPath, InputStream inputStream) throws Exception;

    boolean uploadFile(String targetPath, File file) throws Exception;

    File downloadFile(String targetPath) throws Exception;

    boolean deleteFile(String targetPath) throws Exception;
}
