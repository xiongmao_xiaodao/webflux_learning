package com.xiongmao.webflux.common.sftp;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Author: lao_hu
 * @Date: 2022/2/25 14:12
 * @Description: ftp配置信息读取
 */
@Component
@ConfigurationProperties(ignoreUnknownFields = false, prefix = "sftp.client")
@Data
public class SftpProperties {
    private String host;

    private Integer port;

    private String protocol;

    private String username;

    private String password;

    private String root;

    private String privateKey;

    private String passphrase;

    private String sessionStrictHostKeyChecking;

    private Integer sessionConnectTimeout;

    private Integer channelConnectedTimeout;

}
