package com.xiongmao.webflux.common.sftp;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Author: lao_hu
 * @Date: 2022/2/25 14:05
 * @Description: ftp配置文件读取
 */
@Component
@ConfigurationProperties(ignoreUnknownFields = false, prefix = "filepath.upload")
@Data
@NoArgsConstructor
public class FileConfig {
    private String xUpload;

}
