package com.xiongmao.webflux.common.cookies;

import com.xiongmao.webflux.common.CommonConst;
import org.springframework.http.HttpHeaders;

/**
 * @Author: lao_hu
 * @Date: 2022/2/25 14:11
 * @Description: cookies封装
 */
public class CookiesUtils {
    /**
     * cookies封装
     */
    private CookiesUtils(){

    }
    public static HttpHeaders getCookie(String sid) {
        HttpHeaders headers = new HttpHeaders();
        String cookie = new CookieBuilder().setKey(CommonConst.COOKIES_KEY)
                .setValue(sid)
                .setMaxAge(CommonConst.COOKIES_LIFE)
                .setPath("/")
                .build();
        headers.add("Set-Cookie", cookie);
        return headers;
    }
}
