package com.xiongmao.webflux.common;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.UnsupportedEncodingException;

/**
 * @Author: lao_hu
 * @Date: 2022/2/25 15:31
 * @Description: 异常封装
 */
public class ResponseCollect {


    private ResponseCollect(){
    }

    /**
     * 成功的封装
     */
    public static Mono<Void> successMono(ServerWebExchange serverWebExchange) {
        serverWebExchange.getResponse().setStatusCode(HttpStatus.OK);
        serverWebExchange.getResponse().getHeaders().setContentType(MediaType.APPLICATION_JSON);
        try {
            DataBuffer buffer = serverWebExchange.getResponse()
                    .bufferFactory().wrap(JSON.toJSONString(ResponseData.ok(), SerializerFeature.WriteMapNullValue).getBytes("utf-8"));
            return serverWebExchange.getResponse().writeWith(Flux.just(buffer));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return serverWebExchange.getResponse().writeWith(Flux.just(serverWebExchange.getResponse()
                    .bufferFactory().wrap(JSON.toJSONString(ResponseData.ok(),SerializerFeature.WriteMapNullValue).getBytes())));
        }
    }

    /**
     * 认证异常的封装
     */
    public static Mono<Void> unauthorized(ServerWebExchange serverWebExchange) {
        serverWebExchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);
        serverWebExchange.getResponse().getHeaders().setContentType(MediaType.APPLICATION_JSON);
        try {
            DataBuffer buffer = serverWebExchange.getResponse()
                    .bufferFactory().wrap(JSON.toJSONString(ResponseData.fail(401,"Unauthorized"), SerializerFeature.WriteMapNullValue).getBytes("utf-8"));
            return serverWebExchange.getResponse().writeWith(Flux.just(buffer));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return serverWebExchange.getResponse().writeWith(Flux.just(serverWebExchange.getResponse()
                    .bufferFactory().wrap(JSON.toJSONString(ResponseData.fail(401,"Unauthorized"),SerializerFeature.WriteMapNullValue).getBytes())));
        }
    }

    /**
     * 鉴权异常的封装
     */
    public static Mono<Void> unaccess(ServerWebExchange serverWebExchange, ResponseData data) {
        serverWebExchange.getResponse().setStatusCode(HttpStatus.FORBIDDEN);
        serverWebExchange.getResponse().getHeaders().setContentType(MediaType.APPLICATION_JSON);
        try {
            DataBuffer buffer = serverWebExchange.getResponse()
                    .bufferFactory().wrap(JSON.toJSONString(data, SerializerFeature.WriteMapNullValue).getBytes("utf-8"));
            return serverWebExchange.getResponse().writeWith(Flux.just(buffer));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return serverWebExchange.getResponse().writeWith(Flux.just(serverWebExchange.getResponse()
                    .bufferFactory().wrap(JSON.toJSONString(data,SerializerFeature.WriteMapNullValue).getBytes())));
        }
    }

    /**
     * 认证异常的封装
     */
    public static Mono<Void> failure(ServerWebExchange serverWebExchange, ResponseData data) {
        serverWebExchange.getResponse().setStatusCode(HttpStatus.FORBIDDEN);
        serverWebExchange.getResponse().getHeaders().setContentType(MediaType.APPLICATION_JSON);
        try {
            DataBuffer buffer = serverWebExchange.getResponse()
                    .bufferFactory().wrap(JSON.toJSONString(data, SerializerFeature.WriteMapNullValue).getBytes("utf-8"));
            return serverWebExchange.getResponse().writeWith(Flux.just(buffer));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return serverWebExchange.getResponse().writeWith(Flux.just(serverWebExchange.getResponse()
                    .bufferFactory().wrap(JSON.toJSONString(data,SerializerFeature.WriteMapNullValue).getBytes())));
        }
    }
}
