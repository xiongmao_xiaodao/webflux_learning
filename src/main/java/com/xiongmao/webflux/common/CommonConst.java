package com.xiongmao.webflux.common;

/**
 * @Author: lao_hu
 * @Date: 2022/2/25 13:57
 * @Description:
 */
public interface CommonConst {
    /**
     * 成功标记
     */
    Integer SUCCESS = 200;
    /**
     * 失败标记
     */
    Integer FAIL = 500;
    //===========================================请求路径封装==========================================
    /**
     * 目前往下游分发的请求先暂时写死在这里，后面有时间可以进行更改
     */
    String ignoreUrls = "/aaaa,/bbbb,/cccc";
    /**
     * 登录接口
     */
    String LOGIN_PATH = "/auth/doLogin,/auth/formLogin";
    /**
     * 登出接口
     */
    String LOGOUT_PATH = "/auth/logout";

    //===========================================回话session相关==========================================
    /**
     * 作为登录sessionid 记录到缓存
     */
    String SESSION_ID = "session:id:";
    /**
     * 作为权限信息记入到缓存
     */
    String SESSION_CACHE = "session:cache:";
    /**
     *  Redis默认过期时长，单位：秒
     */
    long DEFAULT_EXPIRE = 60 * 30;
    /**
     * Redis 不设置过期时长
     */
    long NOT_EXPIRE = -1;

    //===========================================Cookies相关==========================================
    /**
     * Cookies 过期时间，毫秒
     */
    long COOKIES_LIFE = 1800000;
    /**
     * Cookies name
     */
    String COOKIES_KEY = "sid";
}
